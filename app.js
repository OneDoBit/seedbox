var http = require('http');
var fs= require('fs');
var index = fs.readFileSync('public/index.html');

var SerialPort = require('serialport');
const parsers = SerialPort.parsers;

const parser = new parsers.Readline({
    delimiter: '\r\n'
});

var port = new SerialPort('COM4', { // Verify Port Connection
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false
});

const port2 = process.env.PORT;



port.pipe(parser);

var app = http.createServer(function(req, res){
   res.writeHead(200, {'Content-Type':'text/html'}); 
    res.end(index);
});

var io = require('socket.io').listen(app);

io.on('connection', function(socket){
    
    
    console.log(port2);
    

});

parser.on('data', function(data) {
    
    console.log('Received data from port: ' + data);
    
    io.emit('data', data);
    
});

app.listen(3000);

//port.pipe(parser);
//
//port.on('open', function(data) {
//console.log('Node is listening to Port');
//    
//});
//
//parser.on('data', function(data) {
//    console.log('Received data from port: ' + data)
//});