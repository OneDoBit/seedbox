
const int pResistor = A0;
const int mResistor = A3;

int value;
int Mvalue;
int i;

void setup(){
 pinMode(pResistor, INPUT);
 pinMode(mResistor, INPUT);
 Serial.begin(9600);
 pinMode(LED_BUILTIN, OUTPUT);
}

void loop(){
  
  Mvalue = analogRead(mResistor);
  
  value = analogRead(pResistor);
  i++;

      /*************************/
      if (Mvalue<=100){ 
        Serial.print("Level: ");
        Serial.println(random(0, 5));
      }
      else if (Mvalue<=400){ 
        Serial.print("Level: ");
        Serial.println(random(5, 20));
      }
      else if (Mvalue>400 && Mvalue <=600){ 
        Serial.print("Level: ");
        Serial.println(random(20, 60));
      }
      else if (Mvalue >=600){ 
        Serial.print("Level: ");
        Serial.println(random(60, 100));
      }

      /*************************/
      Serial.print("Light: ");
      Serial.println(value);

  delay(2000); //Small delay
}
