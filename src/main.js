import Vue from 'vue/dist/vue.js'
import App from './App.vue'
import VueRouter from 'vue-router'
import overview from "./components/MainBody.vue"
import light from "./components/LightBody.vue"
import moisture from "./components/MoistureBody.vue"
import water from "./components/WaterBody.vue"
import type from "./components/PlantType.vue"

Vue.component('overview', overview);
Vue.component('light', light);
Vue.component('moisture', moisture);
Vue.component('water', water);
Vue.component('type', type);

Vue.config.productionTip = false

const routes = [
  { path: '/', component: overview, name: 'home'},
  { path: '/light', component: light, name: 'light'},
  { path: '/moisture', component: moisture, name: 'moisture'},
  { path: '/water', component: water, name: 'water'},
  { path: '/plant-type', component: type, name: 'type'},
]
const router = new VueRouter({
  routes 
})
Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
